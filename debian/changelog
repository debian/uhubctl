uhubctl (2.6.0-1) unstable; urgency=medium

  * New upstream release
  * [7fef7f] Standards version 4.7.0 (no changes)
  * [334b94] Update copyright years

 -- gustavo panizzo <gfa@zumbi.com.ar>  Thu, 05 Sep 2024 02:04:00 +0000

uhubctl (2.5.0-1) unstable; urgency=medium

  * New upstream release
  * [30fd30] Standards version 4.6.1.0 (no changes)
  * [4c4cd2] Update copyright years

 -- gustavo panizzo <gfa@zumbi.com.ar>  Thu, 03 Nov 2022 20:57:54 +0100

uhubctl (2.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- gustavo panizzo <gfa@zumbi.com.ar>  Mon, 05 Apr 2021 14:33:37 +0000

uhubctl (2.3.0-1) unstable; urgency=medium

  [ gustavo panizzo ]
  * New upstream release
  * [30ee12] Standards version 4.5.1 (no changes)
  * [579aa3] Use a version 4 of the watch file
  * [7239e9] Set Rules-Requires-Requires no
  * [d28e61] Refresh patches

 -- gustavo panizzo <gfa@zumbi.com.ar>  Tue, 15 Dec 2020 20:46:37 +0000

uhubctl (2.2.0-1) unstable; urgency=medium

  * New upstream release.
  * [4ef936] Commit Debian 3.0 (quilt) metadata
  * [4fc164] Standards version 4.5.0 (no changes)
  * [2a3c64] Use debhelper 13
  * [5b85ad] Add a patch-header for the automatic quilt patches
  * [d8728d] Removed applied patches to the source code
  * [ad933e] Use the workflow documented in dgit-maint-merge(7),
    update d/README.source to reflect that
  * [c30bf0] Add recommended options to d/source/options from
    dgit-maint-merge(7)
  * [44d9d7] Remove d/gbp.conf

 -- gustavo panizzo <gfa@zumbi.com.ar>  Tue, 02 Jun 2020 20:27:38 +0000

uhubctl (2.1.0-1) unstable; urgency=medium

  * Update to new upstream version 2.1.0.
  * [b36e1d] Standards version 4.4.0 (no changes)
  * [0f6c6d] Use debhelper 12
  * [602741] Add a DEP12 upstream metadata file
  * [554819] Automatically generate the manpage from uhubctl's help

 -- gustavo panizzo <gfa@zumbi.com.ar>  Sun, 08 Sep 2019 01:44:31 +0200

uhubctl (2.0.0-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Use full path to uhubctl binary in tests (Closes: #925913)
  * Mark the uhubctl -v test as superficial
  * Switch from redirecting stderr in script to Test-Command and allow-stderr
  * Use long option --version instead of -v in test

 -- Paul Wise <pabs@debian.org>  Thu, 08 Aug 2019 14:56:30 +0800

uhubctl (2.0.0-5) unstable; urgency=medium

  * Upload to unstable.

 -- gustavo panizzo <gfa@zumbi.com.ar>  Wed, 10 Apr 2019 16:30:47 +0800

uhubctl (2.0.0-4) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Add Vcs-* field

  [ gustavo panizzo ]
  * [fc94ec] Fix autopkgtest

 -- gustavo panizzo <gfa@zumbi.com.ar>  Wed, 03 Apr 2019 09:34:35 +0800

uhubctl (2.0.0-3) unstable; urgency=medium

  * [f88227] Fix autopkgtests
  * [d0629f] New Debian release
  * [883a47] Increasing the compat level to 11
  * [593eba] Bump standards version, no changes were needed
  * [df250c] Change gbp Debian's branch to sid
  * [f37bd9] Document how the packaging is managed in README.source

 -- gustavo panizzo <gfa@zumbi.com.ar>  Mon, 24 Sep 2018 17:25:02 +0800

uhubctl (2.0.0-2) experimental; urgency=medium

  * Noop Debian release

 -- gustavo panizzo <gfa@zumbi.com.ar>  Tue, 31 Jul 2018 10:30:00 +0800

uhubctl (2.0.0-1) unstable; urgency=medium

  * Initial release. (Closes: #892424)

 -- gustavo panizzo <gfa@zumbi.com.ar>  Sun, 11 Mar 2018 14:34:43 +0000
